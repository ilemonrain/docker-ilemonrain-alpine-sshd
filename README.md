## Docker Alpine Linux with OpenSSH-Server  

### 0. 随便说几句
好久不做Docker镜像了……以至于一开始写代码的时候就平白无故的写出了一堆Bug……今天正好是史诗级灾难片《开学》的首映日（大雾），但，自己挖过的坑，命中注定就要自己来维护……所以，我又来创造BUG（划去）更新了（逃  

### 1. 镜像标签 (Tags)
最新版本 ```alpine```, ```alpine:latest```, ```alpine:3.8```  

### 2. 镜像介绍
本镜像，为极度小内存、小磁盘空间环境而生，相较于CentOS和Ubuntu，Alpine更加注重轻量，同时内建了OpenSSH-Server，可以随时通过SSH连接到服务器进行具体配置工作。  

### 3. 使用方法  
拉取镜像：
```
docker pull ilemonrain/alpine-sshd:latest
```

运行镜像：
```
docker run -d -p [外部端口]:22 --name=[容器名称] ilemonrain/alpine-sshd:latest
```

参数说明：
> **-p**：端口映射，冒号前面的是外部端口，冒号后面的为内部端口，根据自己需要自行部署（比如要部署个Nginx，那就是“**-p 22:22 -p 80:80**”）  
> **--name*：容器名称，自行确定，也可以省略（但不方便以后管理）  

 **登录用户名：root 密码：alpine**

 **！！！部署成功后立刻改掉ROOT密码！！！**  
 **！！！部署成功后立刻改掉ROOT密码！！！**  
 **！！！部署成功后立刻改掉ROOT密码！！！**  
 **！！！重要的事情说三遍！！！**  
 **！！！因不改ROOT密码导致容器出现问题，概不负责！！！**  
   
 啥？不会改ROOT密码？  
 **SSH连进去，输入passwd，回车，剩下的再不会，神仙也救不了你！**  

### 4. 更新说明  
 **ilemonrain/alpine-sshd** Update 20180901
 - 重新调整整体代码结构 (你直接说重写一遍好不好 ……)

 **ilemonrain/alpine-sshd** Update 20180605
 - 设置系统默认时区为 Asia/Shanghai
 - 调整Dockerfile结构，使代码更简洁易懂

### 5. 调戏作者（划去）BUG反馈与交流   
**E-mail：** **ilemonrain@ilemonrain.com**  
**Telegram：** @ilemonrain  
**Telegram Channel：** @ilemonrain_channel  